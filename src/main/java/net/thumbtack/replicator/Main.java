package net.thumbtack.replicator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
        final Replicator replicator = new Replicator();
        try {
            replicator.init();
            replicator.start();

            final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.equals("q")) {
                    break;
                }
            }
        } catch (Exception e) {
            LOG.error("FATAL ERROR", e);
        } finally {
            replicator.stop();
        }
    }
}

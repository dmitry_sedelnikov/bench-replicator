package net.thumbtack.replicator;

import com.google.code.or.OpenReplicator;
import com.google.code.or.binlog.impl.ReplicationBasedBinlogParser;
import com.google.code.or.binlog.impl.parser.*;


public class OpenReplicatorCustom extends OpenReplicator {

    @Override
    protected ReplicationBasedBinlogParser getDefaultBinlogParser() throws Exception {

        final ReplicationBasedBinlogParser r = new ReplicationBasedBinlogParser();
        //r.registgerEventParser(new StopEventParser());
        r.registgerEventParser(new RotateEventParser());
        r.registgerEventParser(new IntvarEventParser());
        //r.registgerEventParser(new XidEventParser());
        r.registgerEventParser(new RandEventParser());
        r.registgerEventParser(new QueryEventParser());
        r.registgerEventParser(new UserVarEventParser());
        r.registgerEventParser(new IncidentEventParser());
        r.registgerEventParser(new TableMapEventParser());
        r.registgerEventParser(new WriteRowsEventParser());
        r.registgerEventParser(new UpdateRowsEventParser());
        r.registgerEventParser(new DeleteRowsEventParser());
        r.registgerEventParser(new WriteRowsEventV2Parser());
        r.registgerEventParser(new UpdateRowsEventV2Parser());
        r.registgerEventParser(new DeleteRowsEventV2Parser());
        //r.registgerEventParser(new FormatDescriptionEventParser());

        r.setTransport(this.transport);
        r.setBinlogFileName(this.binlogFileName);
        return r;
    }
}

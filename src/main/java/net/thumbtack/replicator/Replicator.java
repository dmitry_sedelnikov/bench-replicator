package net.thumbtack.replicator;


import com.google.code.or.OpenReplicator;
import com.google.code.or.binlog.BinlogEventListener;
import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.BinlogEventV4Header;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.DatabaseSchema;
import net.thumbtack.replicator.common.PropertiesLoader;
import net.thumbtack.replicator.exception.ParseException;
import net.thumbtack.replicator.exception.StorageException;
import net.thumbtack.replicator.exception.TransformException;
import net.thumbtack.replicator.parser.EventParser;
import net.thumbtack.replicator.storage.KeyValueStorage;
import net.thumbtack.replicator.storage.impl.CouchbaseStorage;
import net.thumbtack.replicator.transformer.Transformer;
import net.thumbtack.replicator.transformer.impl.SimpleTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class Replicator {

    private static final Logger LOG = LoggerFactory.getLogger(Replicator.class);
    private static final String PROPS_FILE_NAME = "mysql.properties";

    private OpenReplicator openReplicator;
    private EventParser eventParser;
    private Set<KeyValueStorage> storages;
    private Transformer transformer;
    private Properties mysqlProps;

    public void init() throws StorageException, IOException {
        transformer = new SimpleTransformer();
        storages = new HashSet<>();
        storages.add(new CouchbaseStorage());
        eventParser = new EventParser();
        openReplicator = new OpenReplicatorCustom();
        mysqlProps = PropertiesLoader.getProperties(PROPS_FILE_NAME);
        openReplicator.setUser(mysqlProps.getProperty("mysql.user"));
        openReplicator.setPassword(mysqlProps.getProperty("mysql.password"));
        openReplicator.setHost(mysqlProps.getProperty("mysql.host"));
        openReplicator.setPort(Integer.parseInt(mysqlProps.getProperty("mysql.port")));
        openReplicator.setServerId(Integer.parseInt(mysqlProps.getProperty("mysql.serverId")));
        openReplicator.setBinlogPosition(Long.parseLong(mysqlProps.getProperty("mysql.binlog.startPosition")));
        openReplicator.setBinlogFileName(mysqlProps.getProperty("mysql.binlog.fileName"));
        openReplicator.setBinlogEventListener(new CustomBinlogEventListener(this));
        /*try {
            long position = restorePosition();
            openReplicator.setBinlogPosition(position);
        } catch (IOException e) {
            LOG.warn("Can't restore replicator's state: " + e.getMessage());
        }*/
    }

    public boolean isRunning() {
        return openReplicator.isRunning();
    }

    public void start() throws Exception {
        openReplicator.start();
        LOG.info("Connected to MySQL: host=" + mysqlProps.getProperty("mysql.host")
            + ":" + mysqlProps.getProperty("mysql.port")
            + "; user=" + mysqlProps.getProperty("mysql.user"));
        LOG.info("Replicator started");
    }

    public void stop(long timeout, TimeUnit timeUnit) {
        if (openReplicator.isRunning()) {
            try {
                openReplicator.stop(timeout, timeUnit);
            } catch (Exception e) {}
        }
        for (KeyValueStorage storage : storages) {
            storage.shutdown();
        }
        LOG.info("Replicator stopped");
    }

    public void stop() {
        stop(0, TimeUnit.MILLISECONDS);
    }


//  ---------------------------------PRIVATE----------------------------------------------------------------------------

    private class CustomBinlogEventListener implements BinlogEventListener {

        private final Replicator replicator;

        public CustomBinlogEventListener(Replicator replicator) {
            this.replicator = replicator;
        }

        @Override
        public void onEvents(BinlogEventV4 event) {
            try {
                BinlogEventV4Header header = event.getHeader();
                long nextPosition = header.getNextPosition();
                LOG.debug("Next position = " + nextPosition);

                List<Change> changes = eventParser.parse(event);
                LOG.debug("CHANGES: " + changes);

                if (changes == null || changes.size() == 0) {
                    return;
                }
                for (Change change : changes) {
                    String key = change.getDatabaseName() + "." + change.getTableName() + "." + change.getPrimaryKey();
                    switch (change.getType()) {
                    case INSERT: {
                        LOG.info("Inserting object with key "
                                + DatabaseSchema.getUniqueName(change.getDatabaseName(), change.getTableName())
                                + "." + change.getPrimaryKey());
                        ByteBuffer dataToStore = transformer.transform(key, change.getColumnsData());
                        for (KeyValueStorage storage : storages) {
                            storage.set(key, dataToStore);
                        }
                        //LOG.debug("Getting object: " + new String(storage.get(key).array()));
                        break;
                    }
                    case UPDATE: {
                        LOG.info("Updating object with key "
                                + DatabaseSchema.getUniqueName(change.getDatabaseName(), change.getTableName())
                                + "." + change.getPrimaryKey());
                        ByteBuffer dataToStore = transformer.transform(key, change.getColumnsData());
                        for (KeyValueStorage storage : storages) {
                            storage.set(key, dataToStore);
                        }
                        break;
                    }
                    case DELETE: {
                        LOG.info("Removing object with key "
                                + DatabaseSchema.getUniqueName(change.getDatabaseName(), change.getTableName())
                                + "." + change.getPrimaryKey());
                        for (KeyValueStorage storage : storages) {
                            storage.remove(key);
                        }
                        break;
                    }
                    default:
                        LOG.error("Unknown change type " + change.getType());
                    }
                }
                storePosition(nextPosition);

            } catch (ParseException e) {
                LOG.error("Error parsing: " + e.getMessage());
                //replicator.stop();
            } catch (TransformException e) {
                LOG.error("Error transform: " + e.getMessage());
                //replicator.stop();
            } catch (StorageException e) {
                LOG.error("Database error: " + e.getMessage());
                //replicator.stop();
            } catch (IOException e) {
                LOG.warn("Error store replicator's state: " + e.getMessage());
            }
        }
    }

    private synchronized void storePosition(long position) throws IOException {
        Properties props = new Properties();
        props.setProperty("position", String.valueOf(position));
        OutputStream out = new FileOutputStream("replicator.state");
        props.store(out, null);
        out.close();
        LOG.debug("Binlog position '" + position + "' saved");
    }

    private long restorePosition() throws IOException {
        Properties props = new Properties();
        InputStream in = new FileInputStream("replicator.state");
        props.load(in);
        in.close();
        String position = props.getProperty("position");
        LOG.debug("Position '" + position + "' restored");
        return Long.parseLong(position);
    }
}
package net.thumbtack.replicator.common;


public enum ChangeType {
    INSERT, UPDATE, DELETE
}

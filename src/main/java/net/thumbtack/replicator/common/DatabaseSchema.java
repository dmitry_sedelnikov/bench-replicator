package net.thumbtack.replicator.common;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DatabaseSchema {

    private String databaseName;
    private String tableName;
    private List<String> columns = new ArrayList<>();
    private String primaryKey;
    private Set<Integer> pkColumnPositions = new HashSet<>();

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Set<Integer> getPkColumnPositions() {
        return pkColumnPositions;
    }

    public void setPkColumnPositions(Set<Integer> pkColumnPositions) {
        this.pkColumnPositions = pkColumnPositions;
    }

    public void addPkColumnPosition(int pkColumnPosition) {
        pkColumnPositions.add(pkColumnPosition);
    }

    public String getUniqueName() {
        return getUniqueName(databaseName, tableName);
    }

    public static String getUniqueName(String databaseName, String tableName) {
        return databaseName + "." + tableName;
    }

    public String toString() {
        return "{database_name=" + databaseName
             + "; table_name=" + tableName
             + "; columns=" + columns
             + "; primary_key=" + primaryKey + "}";
    }
}

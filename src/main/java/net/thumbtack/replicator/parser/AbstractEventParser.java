package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.DatabaseSchema;
import net.thumbtack.replicator.exception.ParseException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractEventParser {

    protected static final Map<String, DatabaseSchema> dbSchemas = new HashMap<>();
    private static final Map<Long, String> idNameMap = new HashMap<>();

    public DatabaseSchema getDatabaseSchema(String name) {
        return dbSchemas.get(name);
    }

    public DatabaseSchema getDatabaseSchema(long id) throws Exception {
        String name = idNameMap.get(id);
        if (name == null) {
            throw new Exception("No database with id " + id);
        }
        return dbSchemas.get(name);
    }

    public void setIdForSchema(long id, String name) {
        idNameMap.put(id, name);
    }

    abstract public int getType();
    abstract public List<Change> parse(BinlogEventV4 event) throws ParseException;

    protected byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(baos);
        os.writeObject(obj);
        return baos.toByteArray();
    }
}

package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.DeleteRowsEvent;
import com.google.code.or.common.glossary.Row;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.ChangeType;
import net.thumbtack.replicator.common.DatabaseSchema;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeleteRowsEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(DeleteRowsEventParser.class);

    private static final int TYPE = MySQLConstants.DELETE_ROWS_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }


    // tableId=34,reserved=1,columnCount=3,usedColumns=111,rows=[Row[columns=[4, 444, hello4]]
    @Override
    public List<Change> parse(BinlogEventV4 eventV4) throws ParseException {
        LOG.debug("Parsing DeleteRowsEvent");
        DeleteRowsEvent event = (DeleteRowsEvent) eventV4;

        long tableId = event.getTableId();
        DatabaseSchema dbSchema;
        try {
            dbSchema = getDatabaseSchema(tableId);
        } catch (Exception e) {
            throw new ParseException(e);
        }

        Set<Integer> pkPositions = dbSchema.getPkColumnPositions();
        List<Change> changes = new ArrayList<>();
        List<Row> rows = event.getRows();
        for (Row row : rows) {
            Change change = new Change(ChangeType.DELETE);
            change.setDatabaseName(dbSchema.getDatabaseName());
            change.setTableName(dbSchema.getTableName());
            StringBuilder keyBuilder = new StringBuilder();
            for (int pkPosition : pkPositions) {
                if (keyBuilder.length() != 0) keyBuilder.append('.');
                keyBuilder.append(row.getColumns().get(pkPosition).toString());
            }
            String keyValue = keyBuilder.toString();
            change.setPrimaryKey(keyValue);
            changes.add(change);
        }
        return changes;
    }
}

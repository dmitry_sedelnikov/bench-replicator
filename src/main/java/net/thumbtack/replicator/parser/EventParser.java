package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.thumbtack.replicator.common.Change;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(EventParser.class);

    private final Map<Integer, AbstractEventParser> parsers;

    public EventParser() {
        parsers = initParsers();
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public List<Change> parse(BinlogEventV4 event) throws ParseException {

        LOG.debug("EVENT: " + event);

        int type =  event.getHeader().getEventType();
        AbstractEventParser parser = parsers.get(type);
        if (null == parser) {
            throw new ParseException("Unknown event type " + event.getClass());
        }
        List<Change> changes = parsers.get(type).parse(event);
        return changes;
    }

    private Map<Integer, AbstractEventParser> initParsers() {

        Map<Integer, AbstractEventParser> parsers = new HashMap<>();
        parsers.put(MySQLConstants.QUERY_EVENT, new QueryEventParser());
        parsers.put(MySQLConstants.WRITE_ROWS_EVENT, new WriteRowsEventParser());
        parsers.put(MySQLConstants.UPDATE_ROWS_EVENT, new UpdateRowsEventParser());
        parsers.put(MySQLConstants.TABLE_MAP_EVENT, new TableMapEventParser());
        parsers.put(MySQLConstants.DELETE_ROWS_EVENT, new DeleteRowsEventParser());
        return parsers;
    }
}

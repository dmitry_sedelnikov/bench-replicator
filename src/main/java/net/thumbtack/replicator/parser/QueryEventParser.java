package net.thumbtack.replicator.parser;


import com.foundationdb.sql.StandardException;
import com.foundationdb.sql.parser.*;
import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.QueryEvent;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.DatabaseSchema;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class QueryEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(QueryEventParser.class);

    private static final int TYPE = MySQLConstants.QUERY_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }

    @Override
    public List<Change> parse(BinlogEventV4 event) throws ParseException {

        QueryEvent queryEvent = (QueryEvent) event;

        //byte[] sql = queryEvent.getSql().getValue();
        String sql = queryEvent.getSql().toString();
        LOG.debug("SQL: " + sql);

        StatementNode stmt;
        try {
            SQLParser parser = new SQLParser();
            stmt = parser.parseStatement(sql);
        } catch (StandardException e) {
            throw new ParseException("SQL = " + sql);
        }

        int nodeType = stmt.getNodeType();
        switch (nodeType) {
            case NodeTypes.CREATE_TABLE_NODE: parseCreateTable(stmt);
            default: return null;
        }
    }

    private void parseCreateTable(StatementNode stmt) {

        DatabaseSchema dbSchema = new DatabaseSchema();
        List<String> columns = new ArrayList<>();
        CreateTableNode createTableNode = (CreateTableNode) stmt;
        TableName tableName = createTableNode.getObjectName();
        dbSchema.setDatabaseName(tableName.getSchemaName());
        dbSchema.setTableName(tableName.getTableName());

        TableElementList tableElementList = createTableNode.getTableElementList();
        for (int i = 0; i < tableElementList.size(); i++) {

            TableElementNode tableElementNode = tableElementList.get(i);
            int nodeType = tableElementNode.getNodeType();

            if (nodeType == NodeTypes.COLUMN_DEFINITION_NODE) {
                columns.add(tableElementNode.getName());
            } else
            if (nodeType == NodeTypes.CONSTRAINT_DEFINITION_NODE) {
                ConstraintDefinitionNode constraintDefinitionNode = (ConstraintDefinitionNode) tableElementNode;
                if (constraintDefinitionNode.getConstraintType() != ConstraintDefinitionNode.ConstraintType.PRIMARY_KEY) {
                    continue;
                }
                ResultColumnList resultColumnList = constraintDefinitionNode.getColumnList();
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < resultColumnList.size(); j++) {
                    ResultColumn resultColumn = resultColumnList.get(j);
                    dbSchema.addPkColumnPosition(resultColumn.getColumnPosition() - 1);
                    if (j != 0) stringBuilder.append('.');
                    stringBuilder.append(resultColumn.getName());
                }
                dbSchema.setPrimaryKey(stringBuilder.toString());
            }
        }
        dbSchema.setColumns(columns);
        dbSchemas.put(dbSchema.getUniqueName(), dbSchema);
        LOG.info("Found database table = " + dbSchema);
    }
}

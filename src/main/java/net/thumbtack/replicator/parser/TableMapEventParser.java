package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.TableMapEvent;
import com.google.code.or.common.util.MySQLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.DatabaseSchema;

import java.util.List;

public class TableMapEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(TableMapEventParser.class);

    private static final int TYPE = MySQLConstants.TABLE_MAP_EVENT;

    @Override
    public int getType() {
        return TYPE;
    }

    @Override
    public List<Change> parse(BinlogEventV4 eventV4) {
        TableMapEvent event = (TableMapEvent) eventV4;
        String dbName = event.getDatabaseName().toString();
        String tableName = event.getTableName().toString();
        String name = DatabaseSchema.getUniqueName(dbName, tableName);
        setIdForSchema(event.getTableId(), name);
        return null;
    }
}

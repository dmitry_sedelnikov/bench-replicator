package net.thumbtack.replicator.parser;


import com.google.code.or.binlog.BinlogEventV4;
import com.google.code.or.binlog.impl.event.UpdateRowsEvent;
import com.google.code.or.common.glossary.Column;
import com.google.code.or.common.glossary.Pair;
import com.google.code.or.common.glossary.Row;
import com.google.code.or.common.glossary.column.BitColumn;
import com.google.code.or.common.util.MySQLConstants;
import net.thumbtack.replicator.common.Change;
import net.thumbtack.replicator.common.ChangeType;
import net.thumbtack.replicator.common.DatabaseSchema;
import net.thumbtack.replicator.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

public class UpdateRowsEventParser extends AbstractEventParser {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateRowsEventParser.class);

    private static final int TYPE = MySQLConstants.UPDATE_ROWS_EVENT;


    @Override
    public int getType() {
        return TYPE;
    }


    // tableId=37,reserved=1,columnCount=3,usedColumnsBefore=111,usedColumnsAfter=111,
    // rows=[Pair[before=Row[columns=[2, 123, hello2]],after=Row[columns=[2, 2, hello2]]]]]
    @Override
    public List<Change> parse(BinlogEventV4 eventV4) throws ParseException {
        UpdateRowsEvent event = (UpdateRowsEvent) eventV4;

        long tableId = event.getTableId();
        DatabaseSchema dbSchema;
        try {
            dbSchema = getDatabaseSchema(tableId);
        } catch (Exception e) {
            throw new ParseException(e);
        }

        List<Change> changes = new ArrayList<>();
        List<Pair<Row>> pairs = event.getRows();
        Set<Integer> pkPositions = dbSchema.getPkColumnPositions();

        for (Pair<Row> pair : pairs) {
            Row row = pair.getAfter();
            BitColumn usedColumns = event.getUsedColumnsAfter();

            Change change = new Change(ChangeType.UPDATE);
            change.setDatabaseName(dbSchema.getDatabaseName());
            change.setTableName(dbSchema.getTableName());

            Map<String, ByteBuffer> columnsData = new HashMap<>();
            StringBuilder pkBuilder = new StringBuilder();
            int i = 0;
            for (Column column : row.getColumns()) {
                String columnName = dbSchema.getColumns().get(i);
                if (usedColumns.get(i)) {
                    byte[] bytes;
                    try {
                        bytes = serialize(column.getValue());
                    } catch (IOException ioe) {
                        throw new ParseException("Serialization error", ioe);
                    }
                    ByteBuffer columnData = ByteBuffer.wrap(bytes);
                    columnsData.put(columnName, columnData);
                    if (pkPositions.contains(i)) {
                        if (pkBuilder.length() != 0) pkBuilder.append('.');
                        pkBuilder.append(column.toString());
                    }
                } else {
                    columnsData.put(columnName, null);
                }
                i++;
            }
            change.setColumnsData(columnsData);
            change.setPrimaryKey(pkBuilder.toString());
            changes.add(change);
        }

        return changes;
    }
}

package net.thumbtack.replicator.storage;

import net.thumbtack.replicator.exception.StorageException;

import java.nio.ByteBuffer;

/**
 * Interface representing abstract key=value storage (to be implemented to every storage)
 */
public interface KeyValueStorage {

    ByteBuffer get(String key) throws StorageException;
    void set(String key, ByteBuffer data) throws StorageException;
    void remove(String key) throws StorageException;
    void shutdown();
}
